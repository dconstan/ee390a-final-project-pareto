# The Pareto Front of Genomic Sequence Alignment
This repository compares accelerator solutions developed in the **PYNQ-Z2** board that perform the **alignment of genomic sequences**. The goal is to obtain a set of **Pareto-optimal solutions for accelerators** developed in the programable logic side of the FPGA. These solutions presentsed in here are obtained by the students from the course **EE-390(a): Lab on hardware-software digital systems codesign**.
## Motivation
Today, researchers count on many algorithms to perform sequence alignment. These algorithms are designed to be extremely efficient since the amount of data to be processed is often in the order of \si{\gibi\byte} or even \si{\tebi\byte}. Thus, execution time has always been an optimization target. However, recent concerns about energy consumption in data centers, both from the environmental and technological points of view, have motivated the switch from **time-to-completion** to **energy-to-completion**.
# Design Space Exploration (PYNQ-Z2)
![solutions](figures/integrated_pynqz2.png)
## Table of solutions (PYNQ-Z2)
| name               | id         |     time |   resources |   energy |   cpu |   brams |   dsps |   luts |   ffs |   brams_per |   dsps_per |   luts_per |   ffs_per |      edp | color   | pareto   |   plevel |    pscore |
|:-------------------|:-----------|---------:|------------:|---------:|------:|--------:|-------:|-------:|------:|------------:|-----------:|-----------:|----------:|---------:|:--------|:---------|---------:|----------:|
| EE1386             | 24-05-24-5 |   10.255 |        5.61 |   14.27  |     0 |       2 |      0 |   9583 |  8761 |        1.79 |       0    |      18.01 |      8.23 |      146 | green   | True     |        1 | 100       |
| EE5584             | 29-05-24-2 |    0.449 |       19.64 |    1.08  |     0 |      12 |      0 |  38084 | 19184 |        8.57 |       0    |      71.59 |     18.03 |        0 | green   | True     |        2 |  97.3684  |
| EE5193             | 29-05-24-0 |    2.263 |       16.52 |    3.49  |     0 |      32 |      6 |  20683 | 19311 |       22.86 |       2.73 |      38.88 |     18.15 |        7 | green   | True     |        1 |  97.3684  |
| EE5247             | 24-05-24-4 |   33.901 |        2.8  |   45.56  |     0 |       1 |      0 |   4625 |  4910 |        0.71 |       0    |       8.69 |      4.61 |     1544 | green   | True     |        1 |  89.4737  |
| EE4195             | 24-05-24-3 |   19.892 |        7.81 |   27.37  |     0 |      19 |      3 |   8708 |  8239 |       13.57 |       1.36 |      16.37 |      7.74 |      544 | red     | False    |        0 |  71.0526  |
| EE3736             | 17-05-24-0 |    7.396 |       33.73 |   11.95  |     0 |       5 |    128 |  42172 | 29398 |        3.57 |      58.18 |      79.27 |     27.63 |       88 | red     | False    |        0 |  68.4211  |
| EE3913             | 16-05-24-0 |  195.91  |        2.39 |  260.17  |     0 |       1 |      4 |   3045 |  3914 |        0.71 |       1.82 |       5.72 |      3.68 |    50969 | green   | True     |        1 |  63.1579  |
| EE2733             | 16-05-24-1 |   58.855 |        3.1  |   79.57  |     0 |       1 |      3 |   4585 |  5104 |        0.71 |       1.36 |       8.62 |      4.8  |     4683 | red     | False    |        0 |  63.1579  |
| EE1579             | 29-05-24-1 |   10.954 |       36.83 |   17.35  |    50 |      40 |      3 |  43050 | 24399 |       28.93 |       1.36 |      80.92 |     22.93 |      190 | red     | False    |        0 |  26.3158  |
| EE7908             | 26-05-24-0 |  138.901 |       22.72 |  220.02  |    50 |      21 |      0 |  21303 |  8736 |       15.36 |       0    |      40.04 |      8.21 |    30560 | red     | False    |        0 |  21.0526  |
| EE2812             | 24-05-24-1 |   85.799 |       24.07 |   85.799 |    50 |      39 |      9 |  14257 | 12351 |       27.86 |       4.09 |      26.8  |     11.61 |     7361 | red     | False    |        0 |  21.0526  |
| EE1825             | 24-05-24-2 |   60.044 |       17.62 |   85.98  |    50 |      11 |      3 |  11471 |  7811 |        7.86 |       1.36 |      21.56 |      7.34 |     5162 | red     | False    |        0 |  21.0526  |
| EE7431             | 24-05-24-0 |  117.703 |       21.28 |  168.55  |    50 |      36 |     14 |   8421 |  8636 |       26.07 |       6.36 |      15.83 |      8.12 |    19838 | red     | False    |        0 |  21.0526  |
| SW_baseline        | 2-5-24-0   |  629.08  |       10    |  910.91  |    50 |       0 |      0 |      0 |     0 |        0    |       0    |       0    |      0    |   573035 | red     | False    |        0 |  13.1579  |
| HW_baseline_driver | 9-5-24-0   | 5217.4   |        2.62 | 7220.88  |     0 |       1 |      4 |   3581 |  4110 |        0.71 |       1.82 |       6.73 |      3.86 | 37674219 | red     | False    |        0 |   7.89474 |
| HW_baseline        | 2-5-24-1   | 5217.4   |       12.63 | 7513.06  |    50 |       1 |      4 |   3585 |  4110 |        0.71 |       1.82 |       6.74 |      3.86 | 39198639 | red     | False    |        0 |   0       |
| EE1036             | 22-05-24-0 |  677.927 |       12.73 |  943.67  |    50 |       1 |      4 |   3781 |  4244 |        0.71 |       1.82 |       7.11 |      3.99 |   639739 | red     | False    |        0 |   0       |
## Understanding the results
**Colors**:
- <span style="color:green">Green</span>: pareto optimal (in at least 2 metrics)
- <span style="color:red">Red</span>: non-pareto optimal
- <span style="color:orange">Star</span>: has a pareto level 3 or 2 (read below)

**Pareto Level** (*plevel*): Indicate the level of pareto optimality in the tree metrics
- 3: better than each other solution in all metrics (only one can exist)
- 2: better than each other solution in at least two metrics (only one can exist)
- 1: better than each other solution in at least one metric
- 0: not pareto optimal

**Pareto Score** (*pscore*): Compares all solutions with each other and gives a score based on the number of metrics in which the solution is better than each of the others
# What about other devices?
![solutions](figures/time_energy_all.png)
## Global Time-Energy Table
| name     |   id | device   |    time |   energy |    edp | color   | pareto   |   plevel |    pscore |
|:---------|-----:|:---------|--------:|---------:|-------:|:--------|:---------|---------:|----------:|
| EE5584   |  113 | pynq     |   0.449 |    1.08  |      0 | green   | True     |        2 | 100       |
| EE5193   |  111 | pynq     |   2.263 |    3.49  |      7 | red     | False    |        0 |  92.3077  |
| EE3736   |  102 | pynq     |   7.396 |   11.95  |     88 | red     | False    |        0 |  75       |
| EE1386   |  109 | pynq     |  10.255 |   14.27  |    146 | red     | False    |        0 |  44.2308  |
| EE1579   |  112 | pynq     |  10.954 |   17.35  |    190 | red     | False    |        0 |  44.2308  |
| EE4195   |  107 | pynq     |  19.892 |   27.37  |    544 | red     | False    |        0 |  42.3077  |
| EE5247   |  108 | pynq     |  33.901 |   45.56  |   1544 | red     | False    |        0 |  40.3846  |
| EE2733   |  101 | pynq     |  58.855 |   79.57  |   4683 | red     | False    |        0 |  32.6923  |
| EE2812   |  105 | pynq     |  85.799 |   85.799 |   7361 | red     | False    |        0 |  30.7692  |
| EE1825   |  106 | pynq     |  60.044 |   85.98  |   5162 | red     | False    |        0 |  32.6923  |
| EE7431   |  104 | pynq     | 117.703 |  168.55  |  19838 | red     | False    |        0 |  30.7692  |
| EE7908   |  110 | pynq     | 138.901 |  220.02  |  30560 | red     | False    |        0 |  30.7692  |
| EE3913   |  100 | pynq     | 195.91  |  260.17  |  50969 | red     | False    |        0 |  25       |
| RPi4_x4  |    3 | RPi      |  37.7   |  527.9   |  19901 | red     | False    |        0 |  32.6923  |
| RPi4_x2  |    2 | RPi      |  75.3   |  760.9   |  57295 | red     | False    |        0 |   3.84615 |
| xeon_x80 |   13 | xeon     |   1.6   |  784     |   1254 | red     | False    |        0 |  69.2308  |
| EE1036   |  103 | pynq     | 677.927 |  943.67  | 639739 | red     | False    |        0 |   1.92308 |
| xeon_x40 |   12 | xeon     |   2.07  | 1010.2   |   2091 | red     | False    |        0 |  38.4615  |
| i9_x20   |    9 | i9       |   3.9   | 1179.4   |   4599 | red     | False    |        0 |  32.6923  |
| RPi4_x1  |    1 | RPi      | 150.4   | 1248.2   | 187729 | red     | False    |        0 |   0       |
| xeon_x20 |   11 | xeon     |   3.45  | 1355.9   |   4677 | red     | False    |        0 |  30.7692  |
| i9_x10   |    8 | i9       |   4.7   | 1440.3   |   6769 | red     | False    |        0 |  21.1538  |
| i9_x8    |    7 | i9       |   5.7   | 1586.8   |   9044 | red     | False    |        0 |  17.3077  |
| i9_x4    |    6 | i9       |  11.1   | 1901.5   |  21106 | red     | False    |        0 |  13.4615  |
| xeon_x10 |   10 | xeon     |   6.77  | 2171.8   |  14703 | red     | False    |        0 |  15.3846  |
| i9_x2    |    5 | i9       |  21.7   | 2652.1   |  57550 | red     | False    |        0 |   9.61538 |
| i9_x1    |    4 | i9       |  44.1   | 4142.9   | 182701 | red     | False    |        0 |   3.84615 |
## Devices details
- RPi4 --> Raspberry Pi 4, x1 thread, x2 threads, x4 threads. Power measured externally for the complete board.
- i9 --> Intel(R) Core(TM) i9-10900K CPU @ 3.70GHz, x1 thread, x2 threads, x4 threads, x8 threads, x10 threads, x20 threads (HT). Power extracted via external power meter, whole machine.
- Xeon --> Dual socket Intel(R) Xeon(R) Gold 6242R CPU @ 3.10GHz, x10 threads, x20 threads, x40 threads, x80 threads (HT). Power extracted via RAPL, includes only processor.
- Parallel versions implemented with OpenMP.

# Sequence Alignment aplication details
In bioinformatics, a [sequence alignment](https://en.wikipedia.org/wiki/Sequence_alignment) is a way of arranging the sequences of DNA, RNA, or protein to identify regions of similarity that may be a consequence of functional, structural, or evolutionary relationships between the sequences.

Sequence alignment is a critical tool in bioinformatics that allows researchers to study the similarity between genomes of different species or to trace possible evolutionary paths. When two sequences for the same protein are aligned, it may be possible to find out individual mutations between them, represented by nucleotide changes. Sequence alignment also exposes possible deletions or insertions of nucleotides in the sequence, by shifting one sequence with respect to the other. An intermediate point in sequence alignment is the determination of the similarity score between two sequences. In this project, we will focus on the generation of the similarity score.

# Metrics
## Latency
The latency on the board is calculated as follows:
```c
clock_gettime(CLOCK_MONOTONIC_RAW, &start);
... // Code to measure
clock_gettime(CLOCK_MONOTONIC_RAW, &end);
elapsedTime = CalcTimeDiff(end, start);
```

Where `CalcTimeDiff` is:
```c
uint64_t CalcTimeDiff(const struct timespec & time2, const struct timespec & time1)
{
  return time2.tv_sec == time1.tv_sec ?
    time2.tv_nsec - time1.tv_nsec :
    (time2.tv_sec - time1.tv_sec - 1) * 1000000000 + (1000000000 - time1.tv_nsec) + time2.tv_nsec;
}
```

## Resources
### PYNQ-Z2
The total cost of the resources is calculated at the percentage of each resource used in the PYNQ-Z2 board and weighted the same. This represents the amount of resources used by a design that cannot be used for other cimputations.

The total amount of resources is:
| Resource         | Value |
|------------------|-------|
| Number of cores  | 2     |
| Number of BRAMs  | 140   |
| Number of DSPs   | 220   |
| Number of LUTs   | 53200 |
| Number of FFs    | 106400|

An the resource cost formula is:
$$
C_R = \frac{20}{100}\times (\%CoreUtilization) + \frac{20}{140} \times (\#BRAMs) + \frac{20}{220} \times (\#DSPs) + \frac{20}{53200} \times (\#LUTs) + \frac{20}{106400} \times (\#FFs)
$$

The numbers of usage of each resource of the programmable logic (BRAMs, DSPs, LUTs, and FFs) is extracted from the implementation report from Vivavdo.

The core utilization is obtain by measure the time the CPU was aprocessing the current task using the follwong code:
```c
clock_gettime(CLOCK_PROCESS_CPUTIME_ID, & startCPUTime);
clock_gettime(CLOCK_MONOTONIC_RAW, &start);
... // Code to measure
clock_gettime(CLOCK_MONOTONIC_RAW, &end);
clock_gettime(CLOCK_PROCESS_CPUTIME_ID, & endCPUTime);
elapsedTime = CalcTimeDiff(end, start);
cpuUtilization = (double)CalcTimeDiff(endCPUTime, startCPUTime) / elapsedTime;
```
The previous code is used to measure both latency and CPU usage. 

## Energy
We use the OTII device to measure energy comsumption of the entire board. The board is supplied externally with 8V with the OTII in series with an external power supply. The OTII device performs a profiling of the input current supplied to the board. To compute the energy, we extract from the OTII measurement the average current over a significant part of the computation, multiply it by the input voltage supply and the elapsed time measured before.

# How to use
## Install dependencies
Python packages
```bash
pip install numpy
pip install pandas
pip install matplotlib
pip install Jinja2
pip install tabulate
```
Alternatively, if you use conda:
```bash
conda create -n dse pandas matplotlib numpy Jinja2 tabulate
conda activate dse
```
## Run
The python script `dse-analysis.py` will run the plots that are displayed on this readme and will save them in the `figures\` folder. The script can be run with:
```bash
python dse-analysis.py
```
### Options
The following flags can be added when executing `dse-analysis.py`:
| Command     | Description                                         |
|:------------|:----------------------------------------------------|
| `--show`    | Show the figures                                    |
| `--readme`  | Update the README.md file with the solutions table  | 
| `--latex`   | Generate latex output                               |

## Input a new datapoint
There are two independent sources of datapoints:
- `solutions_pynqz2.csv` - Time, energy, and resoucres off solutions developed in the PYNQ-Z2
- `solutions_devices.csv` - Tieme and energy of solutions in different devices 
To introduce a new data point on any solution set just add new line corresponding `.csv` file.

For the pynqz2, you can specify either the absolute number of each resources or the percetntage expressed as a number from 0 to 100 under the `*_per` value for each type. The other value will be automatically completed or checked in case you specify both values. When you specify this value (put two decimals only), it will be checked and an error will be rised if there is a missmatch with the computed cost formula.
