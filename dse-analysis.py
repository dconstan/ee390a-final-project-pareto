'''
This script is used to analyze the data from the genome sequencing project for the course ee390a
The data is stored in a csv file and the script reads the data and performs plt the results with three metrcis:
1. Execution time
2. Resource usage
3. Energy consumption

Authors:
- Rubén Rodriguez Alvarez
- Denisa-Andreea Constantinescu
- Miguel Peon Quiros
'''

import argparse

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from math import sqrt

# Argument parser
parser = argparse.ArgumentParser(description='Analyze the data from the genome sequencing project')
parser.add_argument('--show', action="store_true", help='Show the figures')
parser.add_argument('--readme', action="store_true", help='Update the README.md file with the solutions table')
parser.add_argument('--latex', action="store_true", help='Generate latex output')
args = parser.parse_args()

#%% Data pre-processing functions
def get_metrics_table(time_max, resource_max, energy_max):
    '''
    Returns the metrics and resources tables
    Uses the pynq-z2 model
    '''
    # Metrcis table
    metrics = pd.Series(["time","resources","energy"])
    maxims = pd.Series([time_max , resource_max, energy_max]) # These are the limts for the plots
    units = pd.Series(['Execution Time (s)','Resource Usage (%)','Energy Consumption (J)'])
    metric_table = pd.DataFrame({'metric': metrics, 'max': maxims, 'unit': units})

    # Return the tables
    return metric_table

def complete_resources_pynq(sols):
    '''
    Check the data for missing values and compute the missing values
    Computes the resource cost based on the resource model
    '''

    # Resources table
    resources = pd.Series(["brams","dsps","luts","ffs"])
    maxims = pd.Series([140 , 220 , 53200 , 106400])
    resource_table = pd.DataFrame({'resource': resources, 'max': maxims})

    # Set all percentage to a precison of 2 decimals
    sols['brams_per'] = sols['brams_per'].astype(float).round(2)
    sols['dsps_per'] = sols['dsps_per'].astype(float).round(2)
    sols['luts_per'] = sols['luts_per'].astype(float).round(2)
    sols['ffs_per'] = sols['ffs_per'].astype(float).round(2)
    # Fill the missing values
    for sol in range(len(sols)):
        for metric in resource_table['resource']:
            # If both values are defined check if the percentage was correctlpy computed to the second decimal
            if not pd.isna(sols[metric+'_per'][sol]) and not pd.isna(sols[metric][sol]):
                try:
                    assert sols[metric+'_per'][sol] == ((100 * sols[metric][sol] / resource_table.loc[resource_table['resource'] == metric, 'max'].values[0]).round(2))
                except AssertionError:
                    print(f"There is a value missmatch in solution {sols.loc[sol, 'name']} for metric {metric}: {sols[metric+'_per'][sol]} != {(100 * sols[metric][sol] / resource_table.loc[resource_table['resource'] == metric, 'max'].values[0]).round(2)}")
                    assert False
            # Calculate absolute values if missing
            if pd.isna(sols[metric][sol]):
                sols.loc[sol, metric] = int(sols[metric + '_per'][sol] * resource_table.loc[resource_table['resource'] == metric, 'max'].values[0] / 100)
                sols.loc[sol, metric] = sols.loc[sol, metric].astype(int)
            # Calculate percentage values if missing
            if pd.isna(sols[metric+'_per'][sol]):
                sols.loc[sol, metric+'_per'] = (100 * sols[metric][sol] / resource_table.loc[resource_table['resource'] == metric, 'max'].values[0]).round(2)
    # Transform absolute values to integer
    sols['brams'] = sols['brams'].astype(int)
    sols['dsps'] = sols['dsps'].astype(int)
    sols['luts'] = sols['luts'].astype(int)
    sols['ffs'] = sols['ffs'].astype(int)
    # Assert that there are no missing values
    try:
        assert sols.isna().sum().sum() == 0
    except AssertionError:
        print("There are missing values in the data")
        assert False

    return sols

def resource_model_pynq(sols):
    '''
    Computes the resource model for the pynq-z2 board
    '''

    # Calculate the resources model
    resources = 0.2*sols['cpu'] + 0.2*sols['brams_per'] + 0.2*sols['dsps_per'] + 0.2*sols['luts_per'] + 0.2*sols['ffs_per']
    resources = resources.round(2)
    # If the resources exist in the table check if they are the same
    for sol in range(len(sols)):
        if pd.isna(sols.loc[sol, 'resources']):
            sols.loc[sol, 'resources'] = resources.loc[sol]
        else:
            try:
                assert sols.loc[sol, 'resources'] == resources.loc[sol]
            except AssertionError:
                print(f"Resource cost missmatch: {sols.loc[sol, 'resources']} != {resources.loc[sol]}")
                assert False
    
    return sols

def compute_edp(sols):
    '''
    Computes the EDP for the solutions
    '''

    sols['edp'] = (sols['time'] * sols['energy']).astype(int)
    return sols

def eval_pareto(sols, metrics=['time', 'resources', 'energy'], orderKey='pscore', orderAscending=False):
    '''
    Evaluates the array of solutions and determiones wich are pareto optimal and which are not
    Together with the pareto level and pareto score
    Takes as input all the solutions in a pandas dataframe
    Color legend:
    - green: pareto optimal
    - red: non-pareto optimal
    Pareto level: Indicate the level of pareto optimality in the tree metrics
    - 3: better than each other solution in all metrics (only one can exist)
    - 2: better than each other solution in at least two metrics (only one can exist)
    - 1: better than each other solution in at least one metric
    - 0: not pareto optimal
    Pareto score:
    Compares all solutions with each other and gives a score based on the number of metrics in which the solution is better than all the other
    '''
    colors = []
    plevel = []
    pscore = []
    max_pareto = len(metrics)

    for i in range(len(sols)):
        colors.append('red')
        is_pareto = True
        plevel.append(3)
        pscore.append(0)
        for j in range(len(sols)):
            if i == j:
                continue
            plevel_i = max_pareto
            for metric in metrics:
                plevel_i -= sols[metric][j] <= sols[metric][i]
            pscore[i] += plevel_i
            if plevel_i < plevel[i]:
                plevel[i] = plevel_i
            if plevel_i == 0:
                is_pareto = False
                break
        if is_pareto:
            colors[i] = 'green'
    sols['color'] = colors
    sols['pareto'] = sols['color'] == 'green'
    sols['plevel'] = plevel
    # Normalize all pscore to 100
    pscore = np.array(pscore)
    pscore = 100 * (pscore - pscore.min()) / (pscore.max() - pscore.min())
    sols['pscore'] = pscore
    # sort solutions by the given key
    sols = sols.sort_values(by=orderKey, ascending=orderAscending)

    return sols

#%% Plot functions
def create2Daxis(ax, data, xlabel, ylabel, m_table):
    '''
    Create a pareto plot the the solutions with respect to two metrics
    '''
    # Fill the plot with green
    ax.fill_between([0, m_table.loc[m_table['metric'] == xlabel, 'max'].values[0]], 0, m_table.loc[m_table['metric'] == ylabel, 'max'].values[0], color='lightgreen')
    # Fill the area of the pareto solutions with lightgrey
    for i in range(len(data)):
        if data['pareto'][i]:
            ax.fill_between([data[xlabel][i], m_table.loc[m_table['metric'] == xlabel, 'max'].values[0]], data[ylabel][i], m_table.loc[m_table['metric'] == ylabel, 'max'].values[0], color='lightgrey')
    # Draw a big shinny yellow star if the solution plevel is 3 or 2
    for i in range(len(data)):
        if data['plevel'][i] >= 2:
            ax.scatter(data[xlabel][i], data[ylabel][i], color='orange', marker='*', s=300)
    ax.scatter(data[xlabel], data[ylabel], color=data['color'])
    # Add annotations
    for i in range(len(data)):
        ax.text(data[xlabel][i], data[ylabel][i], data["name"][i], color='black',fontsize=8)
    # Lables and titles
    ax.set_xlabel(m_table.loc[m_table['metric'] == xlabel, 'unit'].values[0])
    ax.set_ylabel(m_table.loc[m_table['metric'] == ylabel, 'unit'].values[0])
    # Set limits
    ax.set_xlim([0, m_table.loc[m_table['metric'] == xlabel, 'max'].values[0]])
    ax.set_ylim([0, m_table.loc[m_table['metric'] == ylabel, 'max'].values[0]])
    # Set grid
    ax.grid()

def create3Daxis(ax, data, m_table):
    # Plot green pareto solutions and red non-pareto solutions
    ax.scatter(data['time'], data['resources'], data['energy'], color=data['color'])
    # Draw a big shinny yellow star if the solution plevel is 3 or 2
    for i in range(len(data)):
        if data['plevel'][i] >= 2:
            ax.scatter(data['time'][i], data['resources'][i], data['energy'][i], color='orange', marker='*', s=300)
    # Add annotations
    for i in range(len(data)):
        ax.text(data['time'][i], data['resources'][i], data['energy'][i], data['name'][i], color='black')
    # Set labels
    ax.set_xlabel(m_table.loc[m_table['metric'] == 'time', 'unit'].values[0])
    ax.set_ylabel(m_table.loc[m_table['metric'] == 'resources', 'unit'].values[0])
    ax.set_zlabel(m_table.loc[m_table['metric'] == 'energy', 'unit'].values[0])
    # Set limits
    ax.set_xlim([0, m_table.loc[m_table['metric'] == 'time', 'max'].values[0]]) # Execution time
    ax.set_ylim([0, m_table.loc[m_table['metric'] == 'resources', 'max'].values[0]]) # Resource usage
    ax.set_zlim([0, m_table.loc[m_table['metric'] == 'energy', 'max'].values[0]]) # Energy consumption

def plot2D(sols, metric_0, metric_1, m_table, name_sufix=''):
    '''
    Plot a 2D plot of the solutions with respect two metrics and save the figure
    '''
    fig = plt.figure(figsize=(20, 10))
    ax = fig.add_subplot(111)
    ax.set_title(f'{metric_0} vs {metric_1}')
    create2Daxis(ax, sols, metric_0, metric_1, m_table)
    fig.savefig(f'figures/{metric_0}_{metric_1}{name_sufix}.png')
    return fig, ax

def add_device_lines(fig, ax, sols, name):
    '''
    Add lines between the dots of the same device
    '''

    sols = sols.sort_values(by='time', ascending=True)

    # Create line plot with the rows in the table with the same device
    device = sols['device'].unique()
    colors = plt.cm.tab10(np.linspace(0, 1, len(device)))
    for dev in device:
        temp_sols = sols[sols['device'] == dev]
        ax.plot(temp_sols['time'], temp_sols['energy'], color=colors[np.where(device == dev)[0][0]])
    # Overwirite png
    fig.savefig(f'figures/{name}.png')

def add_edp_curves(fig, ax, sol, name):
    '''
    Add the EDP curves to the plot
    '''

    # Add EDP curves
    edp = sol['time'] * sol['energy']
    time_max = sol['time'].max()*1.05
    energy_max = sol['energy'].max()*1.05
    time = np.linspace(0.1, time_max, 200) # Linepoints to make the courve smooth
    # edp_values = np.linspace(edp.min(), edp.max(), 5) # How many EDP curves are plotted
    # edp_values = np.logspace(np.log10(edp.min()), np.log10(edp.max()), 5) # How many EDP curves are plotted
    edp_values = np.geomspace(edp.min(), edp.max(), 5) # How many EDP curves are plotted
    f = 4
    for edp in edp_values:
        energy = edp / time
        ax.plot(time, energy, linestyle='dashed', color='black', linewidth=0.5)
        ax.text(sqrt(edp/(f*f)), f*sqrt(edp), int(edp), fontsize=8)
    # Overwrite png
    fig.savefig(f'figures/{name}.png')

def plot3D(sols, m_table, name_sufix=''):
    '''
    Plot a 3D plot of the solutions with time, energy and resources
    '''

    # Plot a 3d plot of the solutions
    fig3D = plt.figure(figsize=(20, 10))
    ax = fig3D.add_subplot(111, projection='3d')
    ax.set_title('Complete design space')
    create3Daxis(ax, sols, m_table)
    fig3D.savefig(f'figures/time_energy_resources{name_sufix}.png')

def integrated_plot(sols, m_table, name_sufix=''):
    '''
    Plot an integrated plot with all the metrics
    '''

    # Plot integrated plot
    figTRE = plt.figure(figsize=(24, 20))
    figTRE.suptitle('Time-Resources-Energy Design Space', fontsize=20)

    # Time vs Resources
    axTR = figTRE.add_subplot(221)
    create2Daxis(axTR, sols, 'resources', 'time', m_table)

    # Time vs Energy
    axTE = figTRE.add_subplot(222)
    create2Daxis(axTE, sols, 'energy', 'time', m_table)

    # Energy vs Resources
    axRE = figTRE.add_subplot(224)
    create2Daxis(axRE, sols, 'energy', 'resources', m_table)

    # 3D plot: Time, resources and energy
    axTRE = figTRE.add_subplot(223, projection='3d')
    create3Daxis(axTRE, sols, m_table)
    # Save figure
    figTRE.savefig(f'figures/integrated{name_sufix}.png')

#%% Processing data
# Read the data
solutions_pynqz2 = pd.read_csv('solutions_pynqz2.csv')
solutions_devices = pd.read_csv('solutions_devices.csv')

# Get the metric and resource tables
metric_table = get_metrics_table(time_max=1000, resource_max=40, energy_max=1000)

## Data pre-processing
solutions_pynqz2 = complete_resources_pynq(solutions_pynqz2)
solutions_pynqz2 = resource_model_pynq(solutions_pynqz2)
solutions_pynqz2 = compute_edp(solutions_pynqz2)
solutions_pynqz2 = eval_pareto(solutions_pynqz2)

# Determine the pareto optimal solutions and scores
solutions_devices = compute_edp(solutions_devices)
solutions_devices = eval_pareto(solutions_devices, ['time', 'energy'], 'energy', True)

#%% Plots
# PYNQ integrated plot
integrated_plot(solutions_pynqz2, metric_table, '_pynqz2')                  # Integrated plot

# PYNQ separated plots
plot2D(solutions_pynqz2, 'time', 'resources', metric_table, '_pynqz2')      # Time vs Resources
plot2D(solutions_pynqz2, 'time', 'energy', metric_table, '_pynqz2')         # Time vs Energy
plot2D(solutions_pynqz2, 'energy', 'resources', metric_table, '_pynqz2')    # Energy vs Resources
plot3D(solutions_pynqz2, metric_table, '_pynqz2')                           # 3D plot

# Plot time-energy for all solutions
metric_table = get_metrics_table(time_max=solutions_devices['time'].max()*1.05, resource_max=110, energy_max=solutions_devices['energy'].max()*1.05)
fig_devs, ax_devs = plot2D(solutions_devices, 'time', 'energy', metric_table, '_all')           # Time vs Energy
add_device_lines(fig_devs, ax_devs, solutions_devices, 'time_energy_all')
add_edp_curves(fig_devs, ax_devs, solutions_devices, 'time_energy_all')

#%% Print the solutions
# Print the solutions as latex
if args.latex:
    # Explort pandas dataframe to latex
    with open('latex.tex', 'w') as file:
        file.write(solutions_pynqz2.to_latex(index=False))

# Write the data into the readme
if args.readme:
    # Insert the data in the README.md file section Table of solutions
    with open('template.md', 'r') as file:
        readme = file.read()
        readme = readme.replace('<!-- PYNQ-Z2 TABLE OF SOLUTIONS -->', solutions_pynqz2.to_markdown(index=False))
        readme = readme.replace('<!-- GLOBAL TIME-ENERGY TABLE -->', solutions_devices.to_markdown(index=False))
    with open('README.md', 'w') as file:
        file.write(readme)

# Show figures in pop-up window
if args.show:
    plt.show()
