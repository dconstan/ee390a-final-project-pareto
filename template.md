# The Pareto Front of Genomic Sequence Alignment
This repository compares accelerator solutions developed in the **PYNQ-Z2** board that perform the **alignment of genomic sequences**. The goal is to obtain a set of **Pareto-optimal solutions for accelerators** developed in the programable logic side of the FPGA. These solutions presentsed in here are obtained by the students from the course **EE-390(a): Lab on hardware-software digital systems codesign**.
## Motivation
Today, researchers count on many algorithms to perform sequence alignment. These algorithms are designed to be extremely efficient since the amount of data to be processed is often in the order of \si{\gibi\byte} or even \si{\tebi\byte}. Thus, execution time has always been an optimization target. However, recent concerns about energy consumption in data centers, both from the environmental and technological points of view, have motivated the switch from **time-to-completion** to **energy-to-completion**.
# Design Space Exploration (PYNQ-Z2)
![solutions](figures/integrated_pynqz2.png)
## Table of solutions (PYNQ-Z2)
<!-- PYNQ-Z2 TABLE OF SOLUTIONS -->
## Understanding the results
**Colors**:
- <span style="color:green">Green</span>: pareto optimal (in at least 2 metrics)
- <span style="color:red">Red</span>: non-pareto optimal
- <span style="color:orange">Star</span>: has a pareto level 3 or 2 (read below)

**Pareto Level** (*plevel*): Indicate the level of pareto optimality in the tree metrics
- 3: better than each other solution in all metrics (only one can exist)
- 2: better than each other solution in at least two metrics (only one can exist)
- 1: better than each other solution in at least one metric
- 0: not pareto optimal

**Pareto Score** (*pscore*): Compares all solutions with each other and gives a score based on the number of metrics in which the solution is better than each of the others
# What about other devices?
![solutions](figures/time_energy_all.png)
## Global Time-Energy Table
<!-- GLOBAL TIME-ENERGY TABLE -->
## Devices details
- RPi4 --> Raspberry Pi 4, x1 thread, x2 threads, x4 threads. Power measured externally for the complete board.
- i9 --> Intel(R) Core(TM) i9-10900K CPU @ 3.70GHz, x1 thread, x2 threads, x4 threads, x8 threads, x10 threads, x20 threads (HT). Power extracted via external power meter, whole machine.
- Xeon --> Dual socket Intel(R) Xeon(R) Gold 6242R CPU @ 3.10GHz, x10 threads, x20 threads, x40 threads, x80 threads (HT). Power extracted via RAPL, includes only processor.
- Parallel versions implemented with OpenMP.

# Sequence Alignment aplication details
In bioinformatics, a [sequence alignment](https://en.wikipedia.org/wiki/Sequence_alignment) is a way of arranging the sequences of DNA, RNA, or protein to identify regions of similarity that may be a consequence of functional, structural, or evolutionary relationships between the sequences.

Sequence alignment is a critical tool in bioinformatics that allows researchers to study the similarity between genomes of different species or to trace possible evolutionary paths. When two sequences for the same protein are aligned, it may be possible to find out individual mutations between them, represented by nucleotide changes. Sequence alignment also exposes possible deletions or insertions of nucleotides in the sequence, by shifting one sequence with respect to the other. An intermediate point in sequence alignment is the determination of the similarity score between two sequences. In this project, we will focus on the generation of the similarity score.

# Metrics
## Latency
The latency on the board is calculated as follows:
```c
clock_gettime(CLOCK_MONOTONIC_RAW, &start);
... // Code to measure
clock_gettime(CLOCK_MONOTONIC_RAW, &end);
elapsedTime = CalcTimeDiff(end, start);
```

Where `CalcTimeDiff` is:
```c
uint64_t CalcTimeDiff(const struct timespec & time2, const struct timespec & time1)
{
  return time2.tv_sec == time1.tv_sec ?
    time2.tv_nsec - time1.tv_nsec :
    (time2.tv_sec - time1.tv_sec - 1) * 1000000000 + (1000000000 - time1.tv_nsec) + time2.tv_nsec;
}
```

## Resources
### PYNQ-Z2
The total cost of the resources is calculated at the percentage of each resource used in the PYNQ-Z2 board and weighted the same. This represents the amount of resources used by a design that cannot be used for other cimputations.

The total amount of resources is:
| Resource         | Value |
|------------------|-------|
| Number of cores  | 2     |
| Number of BRAMs  | 140   |
| Number of DSPs   | 220   |
| Number of LUTs   | 53200 |
| Number of FFs    | 106400|

An the resource cost formula is:
$$
C_R = \frac{20}{100}\times (\%CoreUtilization) + \frac{20}{140} \times (\#BRAMs) + \frac{20}{220} \times (\#DSPs) + \frac{20}{53200} \times (\#LUTs) + \frac{20}{106400} \times (\#FFs)
$$

The numbers of usage of each resource of the programmable logic (BRAMs, DSPs, LUTs, and FFs) is extracted from the implementation report from Vivavdo.

The core utilization is obtain by measure the time the CPU was aprocessing the current task using the follwong code:
```c
clock_gettime(CLOCK_PROCESS_CPUTIME_ID, & startCPUTime);
clock_gettime(CLOCK_MONOTONIC_RAW, &start);
... // Code to measure
clock_gettime(CLOCK_MONOTONIC_RAW, &end);
clock_gettime(CLOCK_PROCESS_CPUTIME_ID, & endCPUTime);
elapsedTime = CalcTimeDiff(end, start);
cpuUtilization = (double)CalcTimeDiff(endCPUTime, startCPUTime) / elapsedTime;
```
The previous code is used to measure both latency and CPU usage. 

## Energy
We use the OTII device to measure energy comsumption of the entire board. The board is supplied externally with 8V with the OTII in series with an external power supply. The OTII device performs a profiling of the input current supplied to the board. To compute the energy, we extract from the OTII measurement the average current over a significant part of the computation, multiply it by the input voltage supply and the elapsed time measured before.

# How to use
## Install dependencies
Python packages
```bash
pip install numpy
pip install pandas
pip install matplotlib
pip install Jinja2
pip install tabulate
```
Alternatively, if you use conda:
```bash
conda create -n dse pandas matplotlib numpy Jinja2 tabulate
conda activate dse
```
## Run
The python script `dse-analysis.py` will run the plots that are displayed on this readme and will save them in the `figures\` folder. The script can be run with:
```bash
python dse-analysis.py
```
### Options
The following flags can be added when executing `dse-analysis.py`:
| Command     | Description                                         |
|:------------|:----------------------------------------------------|
| `--show`    | Show the figures                                    |
| `--readme`  | Update the README.md file with the solutions table  | 
| `--latex`   | Generate latex output                               |

## Input a new datapoint
There are two independent sources of datapoints:
- `solutions_pynqz2.csv` - Time, energy, and resoucres off solutions developed in the PYNQ-Z2
- `solutions_devices.csv` - Tieme and energy of solutions in different devices 
To introduce a new data point on any solution set just add new line corresponding `.csv` file.

For the pynqz2, you can specify either the absolute number of each resources or the percetntage expressed as a number from 0 to 100 under the `*_per` value for each type. The other value will be automatically completed or checked in case you specify both values. When you specify this value (put two decimals only), it will be checked and an error will be rised if there is a missmatch with the computed cost formula.
